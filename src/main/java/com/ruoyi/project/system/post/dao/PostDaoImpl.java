package com.ruoyi.project.system.post.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.ruoyi.framework.web.dao.DynamicObjectBaseDao;
import com.ruoyi.framework.web.page.PageUtilEntity;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.post.domain.Post;

/**
 * 岗位信息 数据层实现
 * 
 * @author ruoyi
 */
@Repository("postDao")
public class PostDaoImpl extends DynamicObjectBaseDao implements IPostDao
{
    /**
     * 查询系统操作日志集合
     * 
     * @param pageUtilEntity 分页参数
     * @return 操作日志集合
     */
    @Override
    public TableDataInfo pageInfoQuery(PageUtilEntity pageUtilEntity)
    {
        return this.findForList("SystemPostMapper.pageInfoQuery", pageUtilEntity);
    }

    /**
     * 查询所有岗位
     * 
     * @return 岗位列表
     */
    @Override
    public List<Post> selectPostAll()
    {
        return this.findForList("SystemPostMapper.selectPostAll");
    }

    /**
     * 根据用户ID查询岗位
     * 
     * @param userId 用户ID
     * @return 岗位列表
     */
    @Override
    public List<Post> selectPostsByUserId(Long userId)
    {
        return this.findForList("SystemPostMapper.selectPostsByUserId", userId);
    }

    /**
     * 通过岗位ID查询岗位信息
     * 
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    @Override
    public Post selectPostById(Long postId)
    {
        return this.findForObject("SystemPostMapper.selectPostById", postId);
    }

    /**
     * 通过岗位ID删除岗位信息
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    public int deletePostById(Long postId)
    {
        return this.delete("SystemPostMapper.deletePostById", postId);
    }

    /**
     * 批量删除岗位信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int batchDeletePost(Long[] ids)
    {
        return this.delete("SystemPostMapper.batchDeletePost", ids);
    }

    /**
     * 修改岗位信息
     * 
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public int updatePost(Post post)
    {
        return this.update("SystemPostMapper.updatePost", post);
    }

    /**
     * 新增岗位信息
     * 
     * @param post 岗位信息
     * @return 结果
     */
    @Override
    public int insertPost(Post post)
    {
        return this.update("SystemPostMapper.insertPost", post);
    }
}
